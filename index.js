const express = require("express");

//app - server
const app = express();

const port = 3000;
// Middlewares - software that provides common services and capabilities to application outside of what's offered by the operating system

//Allows your app to read JSON data
app.use(express.json());

//allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));



//[SECTION] ROUTES
// GET method
app.get("/greet", (request, response)=> {
	response.send("Hello form the /greet endpoint")
});

//POST method
app.post("/hello", (request, response)=> {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName} Malord!!!!`)
});


//Simple registration form
let users = [];
app.post("/signup", (request, response) => {
	if (request.body.username !== '' && request.body.password !== '' ) {
		users.push(request.body);
		response.send(`Users ${request.body.username} successfully registerred!`);
	} else {
		response.send("Please input BOTH username and password.");
	}
});

// Simple change password transaction
app.patch("/change-password", (request, response) =>{

	let message;
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;
			break;
		} else {
			message = "User does not exist."
		}
	}


	response.send(message);

});



app.listen(port, () => console.log (`Server running at ${port}`));


// [SECTION] Activity
app.get('/home',(request,response)=>{
	response.send('Welcome to the home page');
})

app.get('/users',(request,response)=>{
	response.send(users);
});

app.delete('/delete-user',(request,response)=>{
	let message;
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1);
			message = `User ${request.body.username} has been deleted.`
			break;
		} else {
			message = `User does not exist.`;
		}

	}
	response.send(message);
})